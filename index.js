// if you need to use the Firebase Admin SDK, uncomment the following:
var Imap = require('imap'),
  inspect = require('util').inspect;

var imap = new Imap({
  user: 'USERNAME',
  password: 'PASSWORD',
  host: 'SERVER',
  port: 143,
  tls: true,

});

function processFetchedEmails(f){
  f.on('message', function (msg, seqno) {
    console.log('Message #%d', seqno);
    var prefix = '(#' + seqno + ') ';
    msg.on('body', function (stream, info) {
      var buffer = '';
      stream.on('data', function (chunk) {
        buffer += chunk.toString('utf8');
      });
      stream.once('end', function () {
        console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
      });
    });
    msg.once('attributes', function (attrs) {
      console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
    });
    msg.once('end', function () {
      console.log(prefix + 'Finished');
    });
  })
}

function fetchLastEmails(nrOfEmailsToFetch,box){
  let f = imap.seq.fetch(box.messages.total+':*', {
    bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
    struct: true
  });
  return f
}


function openInbox(cb) {
  imap.openBox('INBOX', true, cb);
}


imap.once('ready', function () {
  openInbox(function (err, box) {
    if (err) throw err;
    let f = fetchLastEmails(1,box)
    processFetchedEmails(f)
  });
});

imap.on('mail', function(newMsgNr){
  console.log("New messages:"+newMsgNr)
  openInbox((err,box)=>{
    if (err) throw err

    let f = fetchLastEmails(newMsgNr,box)
    processFetchedEmails(f)
  })

})

imap.once('error', function (err) {
  console.log(err);
});

imap.once('end', function () {
  console.log('Connection ended');
});

imap.connect();